import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.*;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer min = null;
        for (Integer nb : liste)
            if (min == null || nb < min) min = nb;
        return min;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        boolean res = true;
        for (T elem : liste)
            if (valeur.compareTo(elem) >= 0) res = false;
        return res;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> res = new ArrayList<>();
        int i1 = 0;
        int i2 = 0;
        while(i1 < liste1.size() && i2 < liste2.size()){
            int diff = liste1.get(i1).compareTo(liste2.get(i2));
            if (diff == 0){
                if( ! res.contains(liste1.get(i1))){
                    res.add(liste1.get(i1));
                }
                ++i1; ++i2;
            }
            else if(diff < 0)
                ++i1;
            else ++i2;
        }
        return res;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> mots = Arrays.asList(texte.split(" "));
        List<String> res = new ArrayList<>();
        for (String elem : mots) if(!elem.equals("")) res.add(elem);
        return res;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        HashMap<String,Integer> dico = new HashMap<>();
        for( String mot : decoupe(texte)){
            if(!dico.containsKey(mot)) dico.put(mot,1);
            else dico.put(mot, dico.get(mot)+1);
        }
        String max = null;
        Integer max_nb = null;
        for( String cle : dico.keySet())
            if(max == null || max_nb < dico.get(cle)) { max = cle; max_nb = dico.get(cle);}
            else
                if(max_nb == dico.get(cle)){
                    List<String> elems = Arrays.asList(max,cle);
                    if(Collections.min(elems) == cle) {max = cle; max_nb = dico.get(cle);}
                }
        return max;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        List<String> pile = new ArrayList<>();
        boolean error = false;
        for(int i = 0 ; i< chaine.length(); ++i){
            if (chaine.charAt(i) == '(') pile.add("(");
            else{
                if(pile.size() == 0) error = true;
                else pile.remove(pile.size()-1);
            }
        }
        if(!error) if(pile.size() != 0) error = true;
        return !error;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
            List<String> pile = new ArrayList<>();
            boolean error = false;
            for(int i = 0 ; i< chaine.length(); ++i){
                if (chaine.charAt(i) == '(' || chaine.charAt(i) == '[') pile.add(""+chaine.charAt(i));
                else{
                    if(pile.size() == 0) error = true;
                    else {

                        if ((pile.get(pile.size()-1).equals("[") && chaine.charAt(i) == ']') || (pile.get(pile.size()-1).equals("(") && chaine.charAt(i) == ')')){
                            pile.remove(pile.size()-1);
                        }
                        else error = true;
                    }
                }
            }
            if(!error) if(pile.size() != 0) error = true;
            return !error;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        List<Integer> zone = new ArrayList<>(liste);
        boolean founded = false;
        while(!founded && (zone.size()>1 || (zone.size() == 1 && zone.get(0) == valeur))){
            int half_pos = (zone.size()> 1 ) ? ((zone.size()-1)/2)+1 : 0;
            if (valeur > zone.get(half_pos)) zone = zone.subList(half_pos, zone.size());
            else if (valeur < zone.get(half_pos)) zone = zone.subList(0, half_pos);
            else {
                founded = true;
            }
        }
        return founded;
    }



}
